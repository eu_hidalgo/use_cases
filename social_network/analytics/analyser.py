import pyspark
from pyspark import SparkContext, SparkConf, SQLContext
from pyspark.sql import SparkSession, Row
from pyspark.sql.functions import explode, col, regexp_replace, split, lit, monotonically_increasing_id, greatest
from graphframes import *

conf = SparkConf().setMaster("local")
sc = SparkContext(conf=conf)
#sc.setCheckpointDir('C:\praca\hidalgo\hpda\checkpoints')
sparkSession = SparkSession.builder.appName("twitter-analyser").getOrCreate()


# Load metis edges
edges = sparkSession.read\
        .text("file:///home/hidalgo/hpda/anon_graph_inner_vegan_20201110.metis")\
    .withColumnRenamed("value", "dst")\
    .withColumn("src", monotonically_increasing_id())
# First row in metis format is metadata, so we drop it
edges = edges.filter(edges.src != 0)
# Create a dataframe of graph vertices
vertices = edges.drop("dst").withColumnRenamed("src", "id")
# Transform adjacency list into list of edges and remove empty rows
edges = edges.withColumn("dst", explode(split("dst", " "))).replace('', 'None')
edges = edges.filter("dst <> 'None'")

# Load retweet data
dataset = sparkSession.read.format("csv") \
    .option("header", "true") \
    .load("file:///home/hidalgo/hpda/retweeters_vegan_20210729.csv")
# Create retweet dataframe and turn it into right adjacency format (list of edges)
retweets = dataset.select("author", "retweets")\
    .filter("retweets <> '[]'")

# Create graph
g = GraphFrame(vertices, edges)

levels = None
# Iterate over every tweet, collecting distances to retweeters
for row in retweets.collect():
    author = row.author
    distance = g.shortestPaths(landmarks=[author])
    distance_list = row.retweets.strip('][').split(', ')
    # Create a dataframe of distances to each retweeter for current author
    distances = [distance.filter(distance.id == retweet).first().distances.get(int(author)) for retweet in distance_list]
    distances = sparkSession.createDataFrame(sc.parallelize(distances), "int")
    # Count n-level followers for each level and populate resulting row
    lvl_row = [{"level"+str(index+1): data[0] for index, data in enumerate(distances.groupby('value').count().select('count').collect())}]
    lvl_row[0].update({"author": author})
    lvl_row = sparkSession.createDataFrame(lvl_row)
    if levels is None:
        levels = lvl_row
    else:
        # Requires Spark 3.1
        #levels = levels.unionByName(sparkSession.createDataFrame(lvl_row), allowMissingColumns=True)
        for c in list(set(lvl_row.columns) - set(levels.columns)): levels = levels.withColumn(c, lit(None))
        for c in list(set(levels.columns) - set(lvl_row.columns)): lvl_row = lvl_row.withColumn(c, lit(None))
        levels = levels.unionByName(lvl_row)

#levels.fillna(0).write.csv('file:///home/hidalgo/hpda/veganlevels', header=True)
#levels2 = sparkSession.read.format("csv").option("header", "true").load("file:///home/hidalgo/hpda/veganlevels2")
# Join the retweet dataset with levels dataframe to obtain results
#results = dataset.join(levels2, ['author'], how='left')
results = dataset.join(levels, ['author'], how='left').fillna(0).distinct()
results.coalesce(1).write.csv('file:///home/hidalgo/hpda/vegananalysed3', header=True)
#results.write.mode("overwrite").csv('/tmp/vegananalysed.csv')
#results.repartition(1).write.csv('file:///home/hidalgo/hpda/vegananalysed.csv', header=True)
#results.write.format('csv').option("header",True).save('file:///home/hidalgo/hpda/vegananalysed')


# Write into HDFS
#df = sparkSession.createDataFrame(g)
#df.write.mode("overwrite").csv("/tmp/followergraph")
#df_load = sparkSession.read.csv('/tmp/followergraph')  
#df_load.show()
